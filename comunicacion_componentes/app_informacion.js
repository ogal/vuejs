Vue.component('hijo', {
    template: //html
    `
    <div class="py-5 bg-success">
        <h4>Componente Hijo: {{ numero }}</h4>
        <h4>Componente Hijo: {{ nombre }}</h4>
        <button class="btn btn-warning" @click="numero++">+</button>
    </div>
    `,
    // Con los props recibimos información
    props: ['numero'],
    // El data retorna un objeto, por eso el return lleva llaves
    data() {
        return {
            nombre: 'Lucía'
        }
    },
    // Una vez que el nombre en el div hijo ya se haya leído, lo mandamos al padre
    mounted() {
        this.$emit('nombreHijo', this.nombre);
    }
});

Vue.component('padre', {
    template: //html
    `
    <div class="p-5 bg-dark text-white">
        <h2>Componente Padre: {{ numeroPadre }}</h2>
        <button class="btn btn-danger" @click="numeroPadre++">+</button>
        {{ nombrePadre }}
        <hijo :numero="numeroPadre" @nombreHijo="nombrePadre = $event"></hijo>
    </div>
    `,
    data() {
        return {
            numeroPadre: 0,
            nombrePadre: ''
        }
    }
});

const app = new Vue({
    el: '#app',
    data: {
        
    },
    methods: {
        
    },
    computed: {
        
    }
})