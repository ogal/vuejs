const app = new Vue({

    el: '#app',
    data: {
        saludo: 'Soy ciclo de vida de Vue'
    },
    // Se ejecuta antes de cargar la app
    beforeCreate() {
        console.log('beforeCreate');
    },
    // Se ejecuta al crear métodos. observadores y eventos, pero aún no accede al DOM
    // Aún no se puede acceder a 'el'
    created() {
        console.log('beforeCreate');
    },
    // Se ejecuta antes de insertar el DOM, antes de que vue inserte datos en el HTML
    beforeMount() {
        console.log('beforeMount');
    },
    // Se ejecuta al insertar el DOM 
    mounted() {
        console.log('mounted');
    },
    // Se ejecutan cuando detectan algun cambio en nuestro HTML 
    // o en nuestra instancia de Vue
    beforeUpdate() {
        console.log('beforeUpdate');
    },
    updated() {
        console.log('updated');
    },
    // Antes de destruir la instancia
    beforeDestroy() {
        console.log('beforeDestroy');
    },
    // Cuando se destruye toda la instancia
    destroyed() {
        console.log('destroyed');
    },

    methods: {
        destruir() {
            this.$destroy();
        }
    }
});