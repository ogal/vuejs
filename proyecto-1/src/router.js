import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      // El path es el equivalente a la URI y el name es lo mismo que en Laravel  
      path: '/servicios',
      name: 'serviciosss',
      component: () => import('./views/Servicios.vue')
    },
    {
      path: '/fotos/:id',
      name: 'fotos',
      component: () => import('./views/Fotos.vue')
    },
  ]
})
