//1. Instanciamos vuejs
//2. Localizamos el objeto a gestionar mediante el ID
const app = new Vue({
    el: '#app',
    data: {
        titulo: 'Hola mundo con vuejs',
        frutas: [ 'Manzana','Pera','Plátano' ],
        paises: [
            {nombre: 'Argentina', provincias: 32},
            {nombre: 'Mexico', provincias: 45},
            {nombre: 'Panama', provincias: 8}
        ],
        nuevoPais : '',
        total: 0
    },
    methods: {
        // Utilizamos this porque queremos acceder a una propiedad de data
        agregarPais () {
            this.paises.push({
                nombre: this.nuevoPais, provincias: 17
            });
            // Reseteamos el contenido del v-model para que no salga mil veces si pulsamos más veces el boton
            this.nuevoPais = ''
        }
    },
    computed: {
        sumarFrutas() {
            this.total = 0;
            for(pais of this.paises){
                this.total += pais.provincias;
            }
            return this.total;
        }
    }
})