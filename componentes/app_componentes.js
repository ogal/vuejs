//1. Instanciamos vuejs
//2. El componente tiene que estar dentro de #app
//3. Si utilizamos templates multilínea, lo meteremos todo dentro de un div
Vue.component('saludo', {
    template: //html
    `
    <div>
        <h1>{{ saludo }}</h1>
        <h3>Hola de nuevo!</h3>
    </div>
    `,
    data(){
        return {
            saludo: 'Hola desde Vue'
        }
    }
});

Vue.component('contador', {
    template: //html
    `
    <div>
        <h3>{{ numero }}</h3>
        <button @click="numero++">+</button>
    </div>
    `,
    data(){
        return {
            numero: 0
        }
    }
});

const app = new Vue({
    el: '#app',
    data: {
        
    },
    methods: {
        
    },
    computed: {
        
    }
})

