const app = new Vue({

    // Propiedad del objeto, lo relaciona con nuestro HTML 
    el: '#app',
    // otro objeto 
    data: {
        titulo: 'GYM con Vue',
        tareas: [], 
        nuevaTarea: ''
    },
    methods: {
        // funcion que se ejecuta al click del boton 
        agregarTarea: function(){
            // console.log('click!', this.nuevaTarea);

            // Meter el contenido del input 
            this.tareas.push({
                nombre: this.nuevaTarea,
                estado: false
            });
            // console.log(this.tareas)
            this.nuevaTarea = '';
            localStorage.setItem('gym-vue', JSON.stringify(this.tareas));
        },
        editarTarea: function(index){
            // console.log('editar', index);
            this.tareas[index].estado = true;
            localStorage.setItem('gym-vue', JSON.stringify(this.tareas));
        },
        eliminar: function(index){
            // eliminamos del array el elemento del que pulsamos la X, toma el index y la cantidad de 
            // elementos que queremos eliminar, que en este caso es solo uno, el elemento del index
            this.tareas.splice(index, 1);
            localStorage.setItem('gym-vue', JSON.stringify(this.tareas));
        },
    },
    // esta funcion se va a ejecutar cuando se cargue nuestro app.js
    // antes que cualquier otro código del documento
    created: function() {
        // accedemos a la base de datos local, de modo que cuando actualicemos la web no se borre lo que hacemos
        // Esto lo vemos en DevTools > Application > Storage
        let datosDB = JSON.parse( localStorage.getItem('gym-vue'));

        if(datosDB == null) {
            this.tareas = [];
        }else{
            this.tareas = datosDB;
        }
    }
});