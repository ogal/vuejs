/**En este ejercicio vamos a mapear el state, que son los 
 * datos que contiene la store de vueX
 * * Como estamos trabajando con CDN, no hace falta importar nada
 */
Vue.component('titulo', {
    template: //html
    `
    <div>
        <h1>Numero: {{ numero }}</h1>
        <hijo></hijo>
    </div>
    `,
    computed: {
        // Dentro del array llamamos a las propiedades del state
        ...Vuex.mapState(['numero'])
    },
});

Vue.component('hijo', {
    // El símbolo del $ que utilizamos dentro del template para llamar 
    // al store solo lo utilizamos (como en Laravel) en el HTML del template
    template: //html
    `
    <div>
        <button @click="aumentar">+</button>
        <button @click="disminuir(2)">-</button>
        <button @click="obtenerCursos">Obtener Cursos</button>
        <h1>Numero: {{ numero }}</h1>
        <ul v-for="item of cursos">
            <li>{{ item.nombre }}</li>
        </ul>
    </div>
    `,
    computed: {
        // Dentro del array llamamos a las propiedades del state
        ...Vuex.mapState(['numero', 'cursos'])
    },
    methods: {
        ...Vuex.mapMutations(['aumentar', 'disminuir']),
        ...Vuex.mapActions(['obtenerCursos'])
    }
});

const store = new Vuex.Store({
    state: {
        numero: 10,
        cursos: []
    },
    mutations: {
        aumentar(state) {
            state.numero ++
        },
        disminuir(state, n) {
            state.numero -= n
        },
        // Esta mutación es ejecutada desde el commit de las 'actions'
        /**Recibe como parametro el state y la constante cursos de la 
         * action obtenerCursos, que tiene los valores del Json
         */
        llenarCursos(state, cursosActions) {
            state.cursos = cursosActions
        }
    },
    actions: {
        obtenerCursos: async function({ commit }) {
            // Llamamos a una API que consume el json cursos
            const data = await fetch('cursos.json');
            // Pedimos que nos devuelva en formato objeto desde nuestra API
            const cursos = await data.json();
            // Recibe un segundo parametro el estado vacío
            commit('llenarCursos', cursos)
        }
    }
});

const app = new Vue({
    el: '#app',
    // Aqui estamos llamando a los datos de Vuex
    store,
    data: {
        
    },
    methods: {
        
    },
    computed: {
        
    }
})